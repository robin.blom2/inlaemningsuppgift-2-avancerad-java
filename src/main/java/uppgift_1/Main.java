package uppgift_1;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        List<Person> personList = List.of(
                new Person("Gunnar", "Male", 15000),
                new Person("Lisa", "Female", 32000),
                new Person("Stina", "Female", 24000),
                new Person("Hans", "Male", 27000),
                new Person("Rickard", "Male", 12000),
                new Person("Sten", "Male", 30000),
                new Person("Felicia", "Female", 9000),
                new Person("Peter", "Male", 36000),
                new Person("Agda", "Female", 10000),
                new Person("Neo", "Male", 40000)
        );

        // 1.1 - Calculate average salary for Males & Females:
        System.out.println(
        personList.stream()
                        .collect(Collectors.groupingBy(Person::getGender, Collectors.averagingDouble(Person::getSalary)))
        );

        // 1.2 - Who has the largest salary:
        personList.stream()
                .sorted(Comparator.comparing(Person::getSalary).reversed())
                .map(Person::getName)
                .findFirst()
                .ifPresent(System.out::println);


        // 1.3 - Who has the lowest salary:
        personList.stream()
                .sorted(Comparator.comparing(Person::getSalary))
                .map(Person::getName)
                .findFirst()
                .ifPresent(System.out::println);
    }
}
