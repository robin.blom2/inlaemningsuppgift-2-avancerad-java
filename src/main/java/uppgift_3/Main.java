package uppgift_3;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        Pattern pattern = Pattern.compile("(?i).*[aeiouy].*[aeiouy].*");

        List<String> listOfWords = List.of("Apa", "sakfråga", "Frukt", "Svetsa", "almanAcka", "Kaliber", "Korv", "Automatization");

        List<String> listOfVowels = listOfWords.stream()
                .filter(word -> pattern.matcher(word).matches()).collect(Collectors.toList());

        System.out.println(listOfVowels);

    }

}
