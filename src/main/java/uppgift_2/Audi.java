package uppgift_2;

public class Audi extends Car{

    @Override
    public void prepareParts() {
        System.out.println("Prepares the parts to build a German Audi");
    }

    @Override
    public void buildCar() {
        System.out.println("Starts building the Audi");
    }

    @Override
    public void colourCar() {
        System.out.println("Finishing up the Audi by painting it");
    }

    @Override
    public void testDrive() {
        System.out.println("Making a quality check of the finished Audi");
    }
}
