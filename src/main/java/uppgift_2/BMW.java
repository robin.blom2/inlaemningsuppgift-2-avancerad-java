package uppgift_2;

public class BMW extends Car{

    @Override
    public void prepareParts() {
        System.out.println("Prepares the parts to build a German BMW");
    }

    @Override
    public void buildCar() {
        System.out.println("Starts building the BMW");
    }

    @Override
    public void colourCar() {
        System.out.println("Finishing up the BMW by painting it");
    }

    @Override
    public void testDrive() {
        System.out.println("Making a quality check of the finished BMW");
    }
}
