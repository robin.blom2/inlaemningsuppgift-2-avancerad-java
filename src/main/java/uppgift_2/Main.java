package uppgift_2;

public class Main {

    public static void main(String[] args) {

        CarFactory swedishCarFactory = new SwedishCarFactory();
        Car volvo = swedishCarFactory.orderCar("Volvo");

        Car saab = swedishCarFactory.orderCar("Saab");

        System.out.println("----------------------");

        CarFactory germanCarFactory = new GermanCarFactory();
        Car audi = germanCarFactory.orderCar("Audi");



    }

}
