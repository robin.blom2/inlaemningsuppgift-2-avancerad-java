package uppgift_2;

public class SwedishCarFactory extends CarFactory{

    @Override
    protected Car createCar(String carType) {
        if(carType.equals("Saab")){
            return new Saab();
        } else if(carType.equals("Volvo")){
            return new Volvo();
        } else{
            return null;
        }
    }
}
