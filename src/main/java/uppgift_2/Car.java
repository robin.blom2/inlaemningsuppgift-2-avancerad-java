package uppgift_2;

public abstract class Car {

    public abstract void prepareParts();
    public abstract void buildCar();
    public abstract void colourCar();
    public abstract void testDrive();

}
