package uppgift_2;

public class Saab extends Car{
    @Override
    public void prepareParts() {
        System.out.println("Prepares the parts to build a Swedish Saab");
    }

    @Override
    public void buildCar() {
        System.out.println("Starts building the Saab");
    }

    @Override
    public void colourCar() {
        System.out.println("Finishing up the Saab by painting it");
    }

    @Override
    public void testDrive() {
        System.out.println("Making a quality check of the finished Saab");
    }
}
