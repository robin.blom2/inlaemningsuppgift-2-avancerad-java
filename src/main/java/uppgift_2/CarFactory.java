package uppgift_2;

public abstract class CarFactory {

    public Car orderCar(String carType){

        Car car = createCar(carType);

        car.prepareParts();
        car.buildCar();
        car.colourCar();
        car.testDrive();

        return car;
    }

    // Factory-method:
    protected abstract Car createCar(String carType);

}
