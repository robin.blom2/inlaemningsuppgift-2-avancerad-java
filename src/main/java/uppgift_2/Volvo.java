package uppgift_2;

public class Volvo extends Car{
    @Override
    public void prepareParts() {
        System.out.println("Prepares the parts to build a Swedish Volvo");
    }

    @Override
    public void buildCar() {
        System.out.println("Starts building the Volvo");
    }

    @Override
    public void colourCar() {
        System.out.println("Finishing up the Volvo by painting it");
    }

    @Override
    public void testDrive() {
        System.out.println("Making a quality check of the finished Volvo");
    }
}
