package uppgift_2;

public class GermanCarFactory extends CarFactory {

    @Override
    protected Car createCar(String carType) {
        if(carType.equals("BMW")){
            return new BMW();
        } else if(carType.equals("Audi")){
            return new Audi();
        } else{
            return null;
        }
    }
}
