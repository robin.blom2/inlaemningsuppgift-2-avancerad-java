package uppgift_4;

import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) {

        Thread thread1 = new Thread(() -> {
            System.out.println(
                    IntStream.rangeClosed(2, 350000)
                            .filter(Main::isPrime)
                            .count()
            );
        });

        Thread thread2 = new Thread(() -> {
            System.out.println(
                    IntStream.rangeClosed(350001, 500000)
                            .filter(Main::isPrime)
                            .count()
            );
        });

        thread1.start();
        thread2.start();

    }

    public static boolean isPrime(int num){
        return IntStream.rangeClosed(2, num / 2).allMatch(i -> num % i != 0);
    }

}
